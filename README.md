# Border Sync
A blugin that lets you sync other blugins, bookmarks, themes, settings, and more across Border installations!

## Support
### BJS Blugin
**Web: No** Web edition does not support Blugins<br>
**W96: Yes** Windows 96 edition is fully supported<br>
**Desktop: No** Border Electron does not support w96 APIs<br>
**Mobile: No** Border Mobile hasn't been developed
### BRX Blugin
**Web: No** Web edition does not support Blugins<br>
**W96: No** Windows 96 edition currently doesn't support BRX<br>
**Desktop: Yes** Runs on versions that support [BRX 1.0](https://gitlab.com/themirrazz/brx-standards/-/blob/main/README.md)
**Mobile: No** Border Mobile will support BRX and BJS once made
